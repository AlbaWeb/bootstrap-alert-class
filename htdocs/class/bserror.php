<?php 

// include DB file here

class bserror {

    public function success($message)
    {

        $content ='<div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    '.$message.'</div>';
        return $content;
    }

    public function danger($message)
    {
        $content ='<div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    '.$message.'</div>';
        return $content;        
    }

    public function warning($message)
    {
        $content ='<div class="alert alert-dismissable alert-warning">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    '.$message.'</div>';
        return $content;        
    }

    public function primary($message)
    {
        $content ='<div class="alert alert-dismissable alert-primary">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    '.$message.'</div>';
        return $content;        
    }

    public function secondary($message)
    {
        $content ='<div class="alert alert-dismissable alert-secondary">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    '.$message.'</div>';
        return $content;        
    }

    public function info($message)
    {
        $content ='<div class="alert alert-dismissable alert-info">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    '.$message.'</div>';
        return $content;        
    }

    public function light($message)
    {
        $content ='<div class="alert alert-dismissable alert-light">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    '.$message.'</div>';
        return $content;        
    }

    public function dark($message)
    {
        $content ='<div class="alert alert-dismissable alert-dark">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    '.$message.'</div>';
        return $content;        
    }
}