<?php 

include('./class/bserror.php');
$bserror = new bserror;
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="@lazysod">

  <title>Bootstrap Dismissable Modal example</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <style>
    body{
      background-color: #c2c2c2;

    }
  </style>
</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <a class="navbar-brand" href="#">Dismissable Modals</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://bitbucket.org/AlbaWeb/">Bitbucket Repo</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="mailto:divinorum2001@gmail.com">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1 class="mt-5">A Bootstrap Alert Class</h1>
        <p class="lead">Simple and quick php class for generating a bootstrap alert.</p>
      </div>
      <div class="col-lg-12 text-center">
        <?php 
          echo '<p>';  
          echo $bserror->success('This is an example of a dismissable <b>Success alert</b>');
          echo '</p>';
          echo '<p>';  
          echo $bserror->danger('This is an example of a dismissable <b>danger alert</b>');
          echo '</p>';
          echo '<p>';  
          echo $bserror->warning('This is an example of a dismissable <b>warning alert</b>');
          echo '</p>';
          echo '<p>';  
          echo $bserror->primary('This is an example of a dismissable <b>primary alert</b>');
          echo '</p>';
          echo '<p>';  
          echo $bserror->secondary('This is an example of a dismissable <b>secondary alert</b>');
          echo '</p>';
          echo '<p>';  
          echo $bserror->info('This is an example of a dismissable <b>info alert</b>');
          echo '</p>';
          echo '<p>';  
          echo $bserror->light('This is an example of a dismissable <b>light alert</b>');
          echo '</p>';
          echo '<p>';  
          echo $bserror->dark('This is an example of a dismissable <b>dark alert</b>');
          echo '</p>';
        ?>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.slim.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
